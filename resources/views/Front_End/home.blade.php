@extends('Front_End.layouts_Front.master')

@section('content')

    <!--/grids-->
    <section class="w3l-grids-3 py-5" id="about">
        <div class="container py-md-5 py-3">
            <div class="row bottom-ab-grids align-items-center">

                <div class="col-lg-6 bottom-ab-left">
                    <h6 class="sub-titlehny">A propos</h6>
                    <h3 class="hny-title">Présentation du CIRAD.</h3>
                    <div class="separatorhny"></div>
                    <p class="my-3">Le CIRAD (Centre d’Investigations Médicales et
                        Radiologie) est un centre médical spécialisé en
                        imagerie moderne et à la pointe de la
                        technologie, situé à Angré 7ème tranche
                        Immeuble TERA. Il propose des services
                        permettant d'établir les diagnostics les plus
                        précis. Il dispose également d’une équipe
                        efficace et expérimentée travaillant activement pour offrir aux patients un accueil de qualité et
                        une prise en charge personnalisée. </p>
                </div>
                <div class="col-lg-6 bottom-ab-right mt-lg-0 mt-5 pl-lg-4">
                    <img src="{{ asset('assets/images/img.jpg') }}" alt="" class="img-fluid">
                </div>

            </div>
        </div>
    </section>
    <!--//grids-->
    <!--/features-->
    <section class="w3l-ab-features py-5">
        <div class="container py-md-5 py-3">
            <div class="row features-w3pvt-main" id="features">
                <div class="col-lg-4 col-md-6 feature-gird">
                    <div class="row features-hny-inner-gd">
                        <div class="col-md-2 col-2 featured_grid_left">
                            <div class="icon-hnynumber">
                                <span class="hnynumber">01</span>
                            </div>
                        </div>
                        <div class="col-md-10 col-10 featured_grid_right_info">
                            <h4><a class="link-hny" href="#url">Médecins qualifiés</a></h4>
                            <p>Nous disposons des médecins hautement qualifiés disposés à rendre vos consultations 
                            meilleures et vous assister en cas de besoin. Votre santé notre préoccupation.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 feature-gird mt-md-0 mt-4">
                    <div class="row features-hny-inner-gd">
                        <div class="col-md-2 col-2 featured_grid_left">
                            <div class="icon-hnynumber">
                                <span class="hnynumber">02</span>
                            </div>
                        </div>
                        <div class="col-md-10 col-10 featured_grid_right_info">
                            <h4><a class="link-hny" href="#url">Service de qualité</a></h4>
                            <p>Garantir un service hospitalier à la hauteur des attentes des patients, tel est notre engagement. Au-dessus de tout, nous priorisons la satisfaction de nos patients.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 feature-gird mt-lg-0 mt-md-5 mt-4">
                    <div class="row features-hny-inner-gd">
                        <div class="col-md-2 col-2 featured_grid_left">
                            <div class="icon-hnynumber">
                                <span class="hnynumber">03</span>
                            </div>
                        </div>
                        <div class="col-md-10 col-10 featured_grid_right_info">
                            <h4><a class="link-hny" href="#url">
                                Technologie de pointe</a></h4>
                            <p>Le CIRAD, c’est avant tout un personnel hautement qualité mais aussi des équipements de haute technologie visant à fournir des résultats précis et fiables.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//features-->
    <!--//news-grids-->
    <section class="w3l-news-sec" id="depart">
        <div class="news-mainhny py-5">
            <div class="container text-center py-lg-3">
                <div class="title-content text-center mb-lg-5 mb-4">
                    <h6 class="sub-titlehny">Nos Services</h6>
                    <h3 class="hny-title">
                        Nos Services & Examens</h3>
                    <div class="separatorhny"></div>
                </div>
            </div>
            <div class="owl-news owl-carousel owl-theme">
                <div class="item">
                    <div class="news-img position-relative">
                        <a href="#"><img src="{{ asset('assets/images/top.jpg') }}" class="img-fluid" alt="news image"></a>
                        <div class="title-wrap">
                            <a href="#">
                                <h4 class="title">mammographie</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="news-img">
                        <a href="#"><img src="{{ asset('assets/images/radio.jpg') }}" class="img-fluid" alt="news image"></a>
                        <div class="title-wrap">
                            <a href="#">
                                <h4 class="title">radiographie</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="news-img">
                        <a href="#"><img src="{{ asset('assets/images/irm.jpg') }}" class="img-fluid" alt="news image"></a>
                        <div class="title-wrap">
                            <a href="#">
                                <h4 class="title">IRM</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="news-img">
                        <a href="#"><img src="{{ asset('assets/images/scanner.jpg') }}" class="img-fluid" alt="news image"></a>
                        <div class="title-wrap">
                            <a href="#">
                                <h4 class="title">scanner</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="news-img">
                        <a href="#"><img src="{{ asset('assets/images/echo.jpg') }}" class="img-fluid" alt="news image"></a>
                        <div class="title-wrap">
                            <a href="#">
                                <h4 class="title">echographie</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="news-img">
                        <a href="#"><img src="{{ asset('assets/images/analyse.jpg') }}" class="img-fluid" alt="news image"></a>
                        <div class="title-wrap">
                            <a href="#">
                                <h4 class="title">analyses medicales</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//news-grids-->
    <!--/contact-->
    <section class="w3l-contact-11 pt-5" id="info">
        <div class="contacts-main pt-lg-5 pt-3">
            <div class="title-content text-center">
                <h6 class="sub-titlehny">Contacts</h6>
                <h3 class="hny-title mb-0">Nous Sommes Là Pour Vous Aider</h3>
                <p class="mb-md-5 mb-4">Avez-vous des suggestions ou voulez-vous simplement avoir des informations? Contactez-nous:</p>
            </div>
            <div class="contant11-top-bg mt-4">
                <div class="container pb-lg-5">
                    <div class="d-grid contact py-lg-5 py-4">
                        <div class="contact-info-left d-grid text-left">
                            <div class="contact-info">
                                <span class="fa fa-map-o" aria-hidden="true"></span>
                                <h4>Visitez-nous au:</h4>
                                <p class="mb-2">CIRAD, L 34, Abidjan
                                    7ème tranche, Centre commercial TERA.</p>

                            </div>
                            <div class="contact-info">
                                <span class="fa fa-phone" aria-hidden="true"></span>
                                <h4>Contactez-nous 24H/24:</h4>
                                <p class="mb-2 conp"> <a href="tel:+1-2345-678-11">+225-2722-509-509 </a></p>
                                <p class="conp"> <a href="tel:+1-2345-678-11">+225-2722-528-528</a></p>
                            </div>
                            <div class="contact-info">
                                <span class="fa fa-envelope-o" aria-hidden="true"></span>
                                <h4>Email 24/7:</h4>
                                <p class="mb-2 conp"><a href="mailto:Preventive@email.com"
                                        class="email">radiologie@cirad.ci</a></p>
                                {{-- <p class="conp"><a href="mailto:Preventive@email.com" class="email">info@cirad.ci</a> --}}
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!--//contact-->

@endsection
