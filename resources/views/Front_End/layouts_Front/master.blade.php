<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">

        <link href="//fonts.googleapis.com/css?family=Nunito:400,600,700&display=swap" rel="stylesheet">
        <!-- Template CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/style-starter.css')}}">


    </head>

    <body>

        @include('Front_End.layouts_Front.navbar')

        @include('Front_End.layouts_Front.main_slider')

        @yield('content')

        @include('Front_End.layouts_Front.footer')


        <!-- //copyright -->
        <!-- Template JavaScript -->
        <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>

        <!-- script for testimonials -->
        <script>
            $(document).ready(function() {
                $('.owl-testimonial').owlCarousel({
                    loop: true,
                    margin: 0,
                    nav: true,
                    responsiveClass: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    autoplayHoverPause: false,
                    responsive: {
                        0: {
                            items: 1,
                            nav: false
                        },
                        480: {
                            items: 1,
                            nav: false
                        },
                        667: {
                            items: 1,
                            nav: true
                        },
                        1000: {
                            items: 1,
                            nav: true
                        }
                    }
                })
            })
        </script>
        <!-- //script for testimonials -->

        <script src="{{asset('assets/js/theme-change.js')}}"></script>

        <!-- js for portfolio lightbox -->
        <!-- libhtbox -->
        <script src="{{asset('assets/js/lightbox-plus-jquery.min.js')}}"></script>
        <!-- libhtbox -->
        <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
        <!-- script for banner slider-->
        <script>
            $(document).ready(function() {
                $('.owl-one').owlCarousel({
                    loop: true,
                    margin: 0,
                    nav: false,
                    responsiveClass: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    autoplayHoverPause: false,
                    responsive: {
                        0: {
                            items: 1,
                            nav: false
                        },
                        480: {
                            items: 1,
                            nav: false
                        },
                        667: {
                            items: 1,
                            nav: true
                        },
                        1000: {
                            items: 1,
                            nav: true
                        }
                    }
                })
            })
        </script>
        <!-- //script -->
        <script>
            $(document).ready(function() {
                $('.owl-news').owlCarousel({
                    stagePadding: 200,
                    loop: true,
                    margin: 30,
                    nav: false,
                    responsiveClass: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    autoplayHoverPause: false,
                    responsive: {
                        0: {
                            items: 1,
                            stagePadding: 40,
                            nav: false
                        },
                        480: {
                            items: 1,
                            stagePadding: 40,
                            nav: true
                        },
                        667: {
                            items: 2,
                            stagePadding: 50,
                            nav: true
                        },
                        1000: {
                            items: 2,
                            nav: true
                        }
                    }
                })
            })
        </script>
        <!-- stats number counter-->
        <script src="{{asset('assets/js/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery.countup.js')}}"></script>
        <script>
            $('.counter').countUp();
        </script>
        <!-- //stats number counter -->
        <!--/MENU-JS-->
        <script>
            $(window).on("scroll", function() {
                var scroll = $(window).scrollTop();

                if (scroll >= 80) {
                    $("#site-header").addClass("nav-fixed");
                } else {
                    $("#site-header").removeClass("nav-fixed");
                }
            });

            //Main navigation Active Class Add Remove
            $(".navbar-toggler").on("click", function() {
                $("header").toggleClass("active");
            });
            $(document).on("ready", function() {
                if ($(window).width() > 991) {
                    $("header").removeClass("active");
                }
                $(window).on("resize", function() {
                    if ($(window).width() > 991) {
                        $("header").removeClass("active");
                    }
                });
            });
        </script>
        <!--//MENU-JS-->

        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    </body>

</html>
