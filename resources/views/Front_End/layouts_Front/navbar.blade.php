<!--header-->
<section class="w3l-header">
    <header id="site-header" class="">
        <section class="w3l-top-header py-3">
            <div class="container">
                <div class="d-grid main-top">
                    <div class="top-header-left">
                        <ul class="info-top-gridshny">
                            <li class="info-grid">
                                <div class="info-icon"><span class="fa fa-clock-o"></span></div>
                                <div class="info-text">
                                    <p>lundi - Vendredi 08:00 - 18:00</p>
                                    <p>Samedi 08:00 - 12:00</p>
                                </div>

                            </li>
                            <li class="info-grid">
                                <div class="info-icon"><span class="fa fa-mobile"></span></div>
                                <div class="info-text">
                                    <p>+ 225 2722 509 509</p>
                                    <p>+ 225 2722 528 528</p>
                                </div>

                            </li>
                            <li class="info-grid">
                                <div class="info-icon"><span class="fa fa-map-marker"></span></div>
                                <div class="info-text">
                                    <p>CIRAD, L 34, Abidjan 7ème tranche</p>
                                    <p>Centre commercial TERA</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="top-header-right text-lg-right">
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/clinique.cirad" target="_blank" ><span class="fa fa-facebook"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="header-2hny py-3">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-dark stroke">
                    <h1>
                        <a class="navbar-brand mr-xl-5" href="index.html">
                            <img src="{{asset('assets/images/Logo_cirad.png')}}" alt="">
                        </a>
                    </h1>
                    <!-- if logo is image enable this
                    <a class="navbar-brand" href="#index.html">
                        <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
                    </a> -->
                    <button class="navbar-toggler  collapsed bg-gradient" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon fa icon-expand fa-bars"></span>
                        <span class="navbar-toggler-icon fa icon-close fa-times"></span>
                        </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mx-lg-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.html">Accueil <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#about">A Propos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#depart">Nos Services</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#info">Nous Contacter</a>
                            </li>
                        </ul>
                        {{-- <ul class="navbar-nav search-right mt-lg-0 mt-2">
                            <li class="nav-item"><a href="#download"
                                    class="free-con btn btn-style btn-secondary d-block" data-toggle="modal"
                                    data-target="#myModal">Prendre Rendez-vous</a>

                                <!-- //modal-popup-->
                                <div class="selectpackage">

                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                                        aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-hidden="true">
                                                        &times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">
                                                        Make An Appointment</h4>
                                                </div>
                                                <div class="modal-body packages">
                                                    <div class="appointment-form">
                                                        <form action="#" method="post">
                                                            <div class="fields-grid">
                                                                <div class="styled-input">

                                                                    <div class="appointment-form-field">

                                                                        <input type="text" name="fullname"
                                                                            placeholder="Full Name" required="">
                                                                    </div>
                                                                </div>
                                                                <div class="styled-input">

                                                                    <div class="appointment-form-field">

                                                                        <input type="text" name="phone"
                                                                            placeholder="Enter Number" required="">
                                                                    </div>
                                                                </div>

                                                                <div class="styled-input">

                                                                    <div class="appointment-form-field">

                                                                        <input type="date" name="age"
                                                                            placeholder="Enter Your Age" required="">
                                                                    </div>
                                                                </div>
                                                                <div class="styled-input">

                                                                    <div class="appointment-form-field">

                                                                        <input type="date" name="date"
                                                                            placeholder="Set a Date" required="">
                                                                    </div>
                                                                </div>
                                                                <div class="styled-input">

                                                                    <div class="appointment-form-field">

                                                                        <select id="department"
                                                                            required="Specialization">
                                                                            <option value="">Specialization*</option>
                                                                            <option value="">Cardiology</option>
                                                                            <option value="">Heart Surgery</option>
                                                                            <option value="">Skin Care</option>
                                                                            <option value="">Body Check-up</option>
                                                                            <option value="">Numerology</option>
                                                                            <option value="">Diagnosis</option>
                                                                            <option value="">Others</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="styled-input">

                                                                    <div class="appointment-form-field">

                                                                        <select id="doctor" required="Select Doctor">
                                                                            <option value="">Select Doctor</option>
                                                                            <option value="">Doctor 1</option>
                                                                            <option value="">Doctor 2</option>
                                                                            <option value="">Doctor 3</option>
                                                                            <option value="">Doctor 4</option>
                                                                            <option value="">Doctor 5</option>
                                                                            <option value="">Doctor 6</option>
                                                                            <option value="">Doctor 7</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="appointment-btn text-lg-right">
                                                                <button type="submit"
                                                                    class="btn btn-style btn-primary mt-4">Book
                                                                    Appointment</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#myModal').modal('show');

                                    </script>
                                </div>
                                <!-- //modal-popup-->
                            </li>
                        </ul> --}}
                        <!-- //toggle switch for light and dark theme -->
                        <!-- search popup -->
                        <div id="search" class="pop-overlay">
                            <div class="popup">
                                <form action="#" method="GET" class="d-sm-flex">
                                    <input type="search" placeholder="Search.." name="search" required="required"
                                        autofocus>
                                    <button type="submit">Search</button>
                                    <a class="close" href="#url">&times;</a>
                                </form>
                            </div>
                        </div>
                        <!-- /search popup -->
                    </div>
                    <!-- //toggle switch for light and dark theme -->
                </nav>
            </div>
        </div>
    </header>
    <!--/header-->
</section>
