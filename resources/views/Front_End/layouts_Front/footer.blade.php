<!--//w3l-subscribe-->
    <!-- footer -->
    <footer class="w3l-footer-29-main">
        <div class="footer-29-w3l py-5">
            <div class="container py-lg-4">
                <div class="row footer-top-29">
                    <div class="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-1 pr-lg-5">
                        <div class="footer-logo mb-3">
                            <a class="footer-brand-logo" href="index.html"><img src="{{asset('assets/images/Logo-CIRAD-miniature.png')}}" alt=""></a>
                        </div>
                        <p>Le CIRAD (Centre d’Investigations Médicales et
                            Radiologie) est un centre médical spécialisé en
                            imagerie moderne et à la pointe de la
                            technologie.</p>
                        <div class="main-social-footer-29 mt-4">
                            <a href="https://www.facebook.com/clinique.cirad" target="_blank" class="facebook"><span class="fa fa-facebook"></span></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-2 mt-sm-0 mt-5">

                        <ul>
                            <h6 class="footer-title-29">Liens utiles</h6>
                            <li><a href="">Accueil</a></li>
                            <li><a href="#about">A propos</a></li>
                            <li><a href="#depart"> Nos services</a></li>
                            <li><a href="#info"> Nous Contacter</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-3 mt-lg-0 mt-5">
                        <h6 class="footer-title-29">Services</h6>
                        <ul>
                            <li><a href="#medicine">Mammographie</a></li>
                            <li><a href="#kidney">Radiographie</a></li>
                            <li><a href="#knee">IRM</a></li>
                            <li><a href="#eye">Scanner</a></li>
                            <li><a href="#surgery">Echographie</a></li>
                            <li><a href="#care">analyses medicales</a></li>

                        </ul>

                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-4 mt-lg-0 mt-5">
                        <h6 class="footer-title-29">Infomation</h6>
                        <p class="mb-3">Voulez-vous avoir des informations sur nos services? Contactez-nous.</p>
                        <p><strong>Addresse :</strong> CIRAD, Abidjan, 7ème tranche, Centre commercial TERA.</p>
                        <p class="my-2"><strong>Contact :</strong> <a href="tel:+12 23456799">+225 2722 509 509</a></p>
                        <p><strong>Email :</strong> <a href="mailto:info@example.com">radiologie@cirad.ci</a></p>


                    </div>
                </div>
            </div>
        </div>
        <!-- //footer -->

        <!-- copyright -->
        <section class="w3l-copyright">
            <div class="container">
                <div class="row bottom-copies">
                    <p class="col-lg-8 copy-footer-29">© 2021 Cirad. All rights reserved. Design by <a href="https://twitter.com/interactivco" target="_blank">
              Interactivco</a></p>

                </div>
            </div>
        </section>
        <!-- move top -->
        <button onclick="topFunction()" id="movetop" title="Go to top">
      &#10548;
    </button>
        <script>
            // When the user scrolls down 20px from the top of the document, show the button
            window.onscroll = function() {
                scrollFunction()
            };

            function scrollFunction() {
                if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                    document.getElementById("movetop").style.display = "block";
                } else {
                    document.getElementById("movetop").style.display = "none";
                }
            }

            // When the user clicks on the button, scroll to the top of the document
            function topFunction() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
            }
        </script>
        <!-- /move top -->
    </footer>
